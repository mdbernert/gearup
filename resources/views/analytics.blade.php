<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <title>PHP Laravel web application</title>
      <link href="{{ asset('css/gradients.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">
    </head>

    <body class="">
        <div class="wrapper">
            <form method="post" action="{{ route('analytics') }}">
                Valores gerados a partir do site https://www.random.org/clock-times
                <br/>
                Insira os valores separados por espaço.
                
                <textarea rows="4" cols="102" name="input">
01:30 01:33 01:47 02:33 02:44 03:21 04:04 04:47 05:04 06:13 07:01 07:04 07:21 07:38 07:43 08:30 08:55 
09:40 09:59 10:48 11:18 11:22 11:48 12:06 12:21 12:49 13:04 13:45 13:50 14:00 14:43 14:56 15:35 16:17 
16:38 17:09 17:27 17:47 18:27 18:35 19:15 19:22 19:33 20:43 21:28 21:31 21:36 22:10 22:11 22:26
                </textarea>
                
                <br/>
                <input type="submit" value="Analisar dados">
            </form>
        </div>
        <script type="text/javascript" src="{{ asset('js/set-background.js') }}"></script>
    </body>
</html>

