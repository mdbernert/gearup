<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class TimeController extends Controller
{
    public function postData(Request $request) {
        $timeArray = explode(' ', trim($request->input));
        $carbArray = [];
        
        
        foreach($timeArray as $time) {
            $timeSegments = explode(':', $time);
            array_push($carbArray, Carbon::createFromTime($timeSegments[0], $timeSegments[1]));
        }
        
        $carbCollection = collect($carbArray)->groupBy(function ($item, $key) {
            return $item->hour;
        });
        
        
        $count = $carbCollection->map(function ($item, $key) {
            return collect($item)->count();
        });;
        
        $max = array_keys($count->all(), max($count->all()));
        
        // $returnThis = array_search(max($arrCompare),$arrCompare)
        
        $media = $count->median();
        
        $maiorIntervalo = $carbArray[0]->diffInMinutes($carbArray[1]);
        $menorIntervalo = $carbArray[0]->diffInMinutes($carbArray[1]);
        
        for ($i = 1; $i <= count($carbArray) - 2; $i++) {
            if($menorIntervalo > $carbArray[$i]->diffInMinutes($carbArray[$i+1]))
                $menorIntervalo = $carbArray[$i]->diffInMinutes($carbArray[$i+1]);
            if($maiorIntervalo < $carbArray[$i]->diffInMinutes($carbArray[$i+1]))
                $maiorIntervalo = $carbArray[$i]->diffInMinutes($carbArray[$i+1]);
            
        }

        
        return response()->json([
            'Número de ocorrências por hora' => $count,
            'Hora com mais ocorrências' => $max,
            'Media de ocorrências por hora' => $media,
            'Maior intervalo' => $maiorIntervalo,
            'Menor intervalo' => $menorIntervalo
        ]);;
    }
}
